package javaapplication2;
import java.util.Scanner;

public class PortoBrechnen {

    public static void main(String[] args) {
        // TODO code application logic here
        int Länge; int Breite; int Höhe; int Gewicht;
        Scanner eingabewert = new Scanner(System.in);
        
        System.out.print("Geben Sie die Länge ein (in mm): ");
        Länge = eingabewert.nextInt();
        System.out.print("Geben Sie die Breite ein (in mm): ");
        Breite = eingabewert.nextInt();
        System.out.print("Geben Sie die Höhe ein (in mm): ");
        Höhe = eingabewert.nextInt();
        System.out.print("Geben Sie die Gewicht ein (in g): ");
        Gewicht = eingabewert.nextInt();
        
        System.out.println(Berechnung(Länge, Breite, Höhe, Gewicht));
        
    }
        public static String Berechnung(int Länge, int Breite, int Höhe, int Gewicht){ 
        
        if (Länge>=140 && Länge<=253 && Breite>=90 && Breite<=125 && Höhe<=5 && Gewicht<=20 ){
            return "Standartbrief. Die Kosten belaufen sich auf 0,70€";
            //System.out.println("Sie haben einen Standartbrief");
        }
        else if (Länge>=100 && Länge<=253 && Breite>=70 && Breite<=125 && Höhe<=10 && Gewicht<=50){
            return "Kompaktbrief. Die Kosten belaufen sich auf 0,85€";
            //System.out.println("Sie haben einen Kompaktbrief");
        }
        else if(Länge>=100 && Länge<=353 && Breite>=70 && Breite<=250 && Höhe<=20 && Gewicht<=500){
            return "Großbrief. Die Kosten belaufen sich auf 1,45€";
            //System.out.println("Sie haben einen Großbrief");
        }
        else if(Länge>=100 && Länge<=353 && Breite>=70 && Breite<=300 && Höhe<=50 && Gewicht<=1000){
            return "Maxibrief. Die Kosten belaufen sich auf 2,60€";
            //System.out.println("Sie haben einen Maxibrief");
        }
        else if(Länge>=100 && Länge<=600 && Breite>=70 && Breite<=300 && Höhe<=150 && Gewicht<=2000 || Länge+Breite+Höhe==900 && Länge<=600 && Breite<=600 && Höhe<=600 && Gewicht<=2000){
            return "MaxibriefPlus. Die Kosten belaufen sich auf 4,80€";
            //System.out.println("Sie haben einen Maxibrief Plus");
        }
        else {
            return "Ungültig. Brief kann nicht klassifiziert werden.";
            //System.out.println("Ihr Brief kann nicht klassifiziert werden");
        }
    }
}

